package main

import (
	"../pokergame"
	"bufio"
	"fmt"
	"net"
	"strconv"
)

var gameRoom pokergame.GameRoom

func main(){
	gameRoom.Size = 3;
	fmt.Println(strconv.Itoa(123))
	gameRoom.Status = "Waiting for players"
	listner, err := net.Listen("tcp", ":8080")
	if err != nil {
		panic(err)
	}
	for {
		conn, err := listner.Accept()
		if err != nil {
			panic(err)
		}
		scanner := bufio.NewScanner(conn)
		fmt.Printf("New Connection!\n")
		conn.Write([]byte("Enter your name:\n"))
		var text string
		var player pokergame.Player
		for scanner.Scan() {
			text = scanner.Text()
			player = pokergame.Player{text, conn}
			break
		}
		go gameRoom.AddPlayer(player)
	}
}
