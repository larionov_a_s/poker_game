package pokergame

import (
	"bufio"
	"fmt"
	"math/rand"
	"net"
	"strconv"
	"time"
)

type Card struct{
	suit string
	value string
}

type Player struct{
	Name string
	Conn net.Conn
}

type InGamePlayer struct{
	Player
	score int
	cardsInHand []Card
}

type GameRoom struct{
	Size int
	Status string
	players []Player
	game Game
}

type Game struct{
	players []InGamePlayer
	turnType TurnType
	enteringPlayerId int
	prevTurnWinnerId int
}

type TurnType struct{
	cardForEach int
	gameType string
}

func (g *Game) AddPlayer(p Player){
	player := new (InGamePlayer)
	player.Name = p.Name
	player.Conn = p.Conn
	player.score = 0
	g.players = append(g.players, *player)
	p.Conn.Write([]byte("You added to the game!\n\r"))
}

func (g *Game) TurnerIndex(i int) int{
	i++
	if i >= len(g.players){
		i = 0;
	}
	return i;
}

func (p *InGamePlayer) WaitCard() Card{
	scanner := bufio.NewScanner(p.Conn)
	for scanner.Scan() {
		cardIndex := scanner.Text()
		cardIndexInt, err := strconv.Atoi(cardIndex)
		if err == nil{
			card := p.cardsInHand[cardIndexInt-1]
			p.cardsInHand = append(p.cardsInHand[:cardIndexInt-1], p.cardsInHand[cardIndexInt:]...)
			return card
		}
	}
	return Card{}
}

func (g *Game) DiscplayCards(){
	for j := 0; j < len(g.players); j++ {
		var request string
		for i := 0; i < len(g.players[j].cardsInHand); i++ {
			request += string(i+1) + ") " + g.players[j].cardsInHand[i].suit + g.players[j].cardsInHand[i].value + " "
		}
		request += "\n\r"
		g.players[j].Conn.Write([]byte(request))
	}
}

func (g *Game) SendAll(msg string){
	for j := 0; j < len(g.players); j++ {
		g.players[j].Conn.Write([]byte(msg))
	}
}



func (g *Game) Start(){
	fmt.Printf("Game has benn started!\n\r")
	g.enteringPlayerId = 0
	g.prevTurnWinnerId = 0
	g.turnType = TurnType{1, "usual"}
	for g.turnType.gameType != "end"{
		for k := 0; k < g.turnType.cardForEach; k++ {
			fmt.Print("Turn number ")
			fmt.Println(g.turnType.cardForEach)
			//раздача карт
			rand.Seed(time.Now().Unix())
			for i := 0; i < len(g.players); i++ {
				for j := 0; j < g.turnType.cardForEach; j++ {
					card := Card{"D", strconv.Itoa(rand.Intn(13) + 1)}
					g.players[i].cardsInHand = append(g.players[i].cardsInHand, card)
				}
			}

			//Игра каждой раздачи
			for l:= 0; l < g.turnType.cardForEach; l++ {
				//Игра одной взятки
				g.DiscplayCards()
				var thisTurnCards []Card
				j := g.prevTurnWinnerId;
				for i := 0; i < len(g.players); j = g.TurnerIndex(j) {
					thisTurnCards = append(thisTurnCards, g.players[j].WaitCard())
					g.SendAll(g.players[j].Name + " throws card " + thisTurnCards[i].suit + thisTurnCards[i].value + "\n\r")
					i++
				}
			}
			g.prevTurnWinnerId = g.TurnerIndex(g.prevTurnWinnerId)
			g.turnType.cardForEach++
		}
		g.enteringPlayerId++
	}
}

func (gr *GameRoom) AddPlayer(p Player){
	if (gr.Status == "Waiting for players"){
		gr.players = append(gr.players, p)
		gr.game.AddPlayer(p)
		if len(gr.players) == gr.Size{
			gr.Status = "Game started"
			go gr.game.Start()
		}
	}
}